/*
    This file is part of tocraft_core.

    tocraft_core is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    tocraft_core is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    http://www.gnu.org/licenses/gpl-3.0.en.html
 */
package pl.mrgregorix.jsonchatapi;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.lang.Validate;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.scoreboard.Objective;

public class ComponentPart {
    private JsonObject object = new JsonObject();

    ComponentPart() {
    } //can be created only by component

    public void setText(String text) {
        object.addProperty("text", text);
    }

    public void setColor(ChatColor color) {
        Validate.isTrue(color.isColor() || color == ChatColor.RESET, "Given format, expected color");
        object.addProperty("color", color.name().toLowerCase());
    }

    public void setFormat(ChatColor format) {
        Validate.isTrue(format.isFormat(), "Given color, expected format");
        object.addProperty(format != ChatColor.MAGIC ? format.name().toLowerCase() : "obfuscated", true);
    }

    public void setTranslation(String translation, String... parameters) {
        object.addProperty("translate", translation);
        if (parameters.length != 0) {
            JsonArray array = new JsonArray();
            for (String param : parameters) {
                array.add(ChatComponent.parser.parse(param));
            }
            object.add("with", array);
        }
    }

    public void setHoverEvent(String action, String parameter) {
        JsonObject hoverEvent = new JsonObject();
        hoverEvent.addProperty("action", action);
        hoverEvent.addProperty("value", parameter);

        object.add("hoverEvent", hoverEvent);
    }

    public void setClickEvent(String action, String parameter) {
        JsonObject hoverEvent = new JsonObject();
        hoverEvent.addProperty("action", action);
        hoverEvent.addProperty("value", parameter);

        object.add("clickEvent", hoverEvent);
    }

    public void setSelector(ChatSelector selector) { //i don't know what selectors doing
        object.addProperty("selector", "@" + selector.getChar());
    }

    public void setScore(OfflinePlayer player, Objective objective) {
        JsonObject o = new JsonObject();
        o.addProperty("playername", player == null ? "*" : player.getName());
        o.addProperty("objective", objective.getName());
        object.add("score", o);
    }

    public void setInsertion(String insertion) {
        object.addProperty("insertion", insertion);
    }

    @Override
    public String toString() {
        return ChatComponent.gson.toJson(object);
    }

    public JsonElement getJson() {
        return object;
    }
}
