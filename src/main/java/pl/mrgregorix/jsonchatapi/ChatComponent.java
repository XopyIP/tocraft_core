/*
    This file is part of tocraft_core.

    tocraft_core is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    tocraft_core is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    http://www.gnu.org/licenses/gpl-3.0.en.html
 */
package pl.mrgregorix.jsonchatapi;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.Objective;
import pl.mrgregorix.jsonchatapi.utils.PacketUtils;
import pl.mrgregorix.jsonchatapi.utils.ReflectionUtils;

import java.util.List;

public final class ChatComponent {
    static Gson gson = new GsonBuilder().create();
    static JsonParser parser = new JsonParser();
    private JsonArray json = new JsonArray();
    private ComponentPart currentPart;
    private List<ComponentPart> parts = Lists.newArrayList();

    public ChatComponent(String text) {
        currentPart = new ComponentPart();
        currentPart.setText(text);
    }

    public static ChatComponent fromText(String message) {
        return fromText(message, '&');
    }

    public static ChatComponent fromText(String message, char colorChar) {
        ChatComponent c = new ChatComponent("");

        char characters[] = message.toCharArray();

        ChatColor lastColor = ChatColor.RESET;
        String text = "";
        for (int i = 0; i < characters.length; i++) {
            char ch = characters[i];
            char ch2 = i == characters.length - 1 ? 0 : characters[i + 1];
            ChatColor color;
            if (ch == colorChar && ch2 != 0 && (color = ChatColor.getByChar(ch2)) != null) {
                c.text(text);
                if (lastColor.isFormat())
                    c.format(lastColor);
                else
                    c.color(lastColor);
                lastColor = color;
                text = "";
                i++;
                continue;
            }
            text += ch;
        }
        c.text(text);
        c.color(lastColor);

        return c.build();
    }

    public ChatComponent text(String text) {
        next();
        currentPart.setText(text);
        return this;
    }

    public ChatComponent color(ChatColor color) {
        currentPart.setColor(color);
        return this;
    }

    public ChatComponent format(ChatColor format) {
        currentPart.setFormat(format);
        return this;
    }

    public ChatComponent translation(String translation, String... params) {
        next();
        currentPart.setTranslation(translation, params);
        return this;
    }

    public ChatComponent selector(ChatSelector selector) {
        next();
        currentPart.setSelector(selector);
        return this;
    }

    public ChatComponent score(OfflinePlayer player, Objective objective) {
        next();
        currentPart.setScore(player, objective);
        return this;
    }

    public ChatComponent insertion(String text) {
        currentPart.setInsertion(text);
        return this;
    }

    public ChatComponent newLine() {
        text("\n");
        return this;
    }

    public ChatComponent clickOpenURL(String url) {
        currentPart.setClickEvent("open_url", url);
        return this;
    }

    public ChatComponent clickOpenFile(String file) {
        currentPart.setClickEvent("open_file", file);
        return this;
    }

    public ChatComponent clickRunCommand(String command) {
        currentPart.setClickEvent("run_command", command);
        return this;
    }

    public ChatComponent clickSuggestText(String text) {
        currentPart.setClickEvent("suggest_command", text);
        return this;
    }

    public ChatComponent hoverShowText(String text) {
        currentPart.setHoverEvent("show_text", text);
        return this;
    }

    public ChatComponent hoverShowAchievement(String achievement) {
        currentPart.setHoverEvent("show_achievement", achievement);
        return this;
    }

    public ChatComponent hoverShowItem(ItemStack item) {
        Object nmsItemStack = ReflectionUtils.invokePrivateMethod(ReflectionUtils.getBukkitClass("inventory.CraftItemStack"), "asNMSCopy", null, new Class[]{ItemStack.class}, item);
        Object tagCompound = ReflectionUtils.createInstance(ReflectionUtils.getNMSClass("NBTTagCompound"), new Class[0]);
        ReflectionUtils.invokePrivateMethod(nmsItemStack.getClass(), "save", nmsItemStack, new Class[]{tagCompound.getClass()}, tagCompound);

        currentPart.setHoverEvent("show_item", tagCompound.toString());
        return this;
    }

    public ChatComponent build() {
        if (currentPart == null)
            throw new IllegalStateException("Component already build");
        parts.add(currentPart);
        currentPart = null;
        for (ComponentPart part : parts)
            json.add(part.getJson());
        return this;
    }

    private void next() {
        parts.add(currentPart);
        currentPart = new ComponentPart();
    }

    public Object toLegacy() {
        Class clazz = ReflectionUtils.getNMSClass("IChatBaseComponent$ChatSerializer");
        return ReflectionUtils.invokePrivateMethod(clazz, "a", null, new Class[]{String.class}, toString());
    }

    public void send(Player player) {
        if (currentPart != null)
            throw new IllegalStateException("Component must be buiilded before send");
        Object packet = PacketUtils.createPacketUsingSingleCtor("PacketPlayOutChat", toLegacy(), (byte) 1);
        PacketUtils.sendPacket(packet, player);
    }

    public void sendToActionBar(Player player) {
        if (currentPart != null)
            throw new IllegalStateException("Component must be buiilded before send");
        Object packet = PacketUtils.createPacketUsingSingleCtor("PacketPlayOutChat", toLegacy(), (byte) 2);
        PacketUtils.sendPacket(packet, player);
    }

    @Override
    public String toString() {
        return gson.toJson(json);
    }
}
