package pl.tocraft.core.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffectType;
import pl.tocraft.core.CorePlugin;
import pl.tocraft.core.McPlayer;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        McPlayer mcPlayer = CorePlugin.getMcPlayer(player);

        event.setQuitMessage(null);

        if (mcPlayer.isVanished()) {
            mcPlayer.setVanished(false);
            player.removePotionEffect(PotionEffectType.INVISIBILITY);
        }
    }
}
