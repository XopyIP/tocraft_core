package pl.tocraft.core.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import pl.tocraft.core.CorePlugin;

public class EntityDeathListener implements Listener {

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        //Player
        Player player = event.getEntity().getKiller();
        if (player != null) {
            CorePlugin.givePlayerDrop(event.getDrops(), player);
            player.giveExp(event.getDroppedExp());
            event.getDrops().clear();
            event.setDroppedExp(0);
            // Money drop :> from mobs
            switch (event.getEntityType()) {
                case ZOMBIE:
                    CorePlugin.economy.depositPlayer(player, 5);
                    break;
                case SKELETON:
                    CorePlugin.economy.depositPlayer(player, 7);
                    break;
                case SPIDER:
                    CorePlugin.economy.depositPlayer(player, 4);
                    break;
                case CAVE_SPIDER:
                    CorePlugin.economy.depositPlayer(player, 8);
                    break;
                case BAT:
                    CorePlugin.economy.depositPlayer(player, 0.5);
                    break;
                case GIANT:
                    CorePlugin.economy.depositPlayer(player, 300);
                    break;
                case SQUID:
                    CorePlugin.economy.depositPlayer(player, 10);
                    break;
                case GHAST:
                    CorePlugin.economy.depositPlayer(player, 70);
                    break;
                case HORSE:
                    CorePlugin.economy.depositPlayer(player, 10);
                    break;
                case BLAZE:
                    CorePlugin.economy.depositPlayer(player, 50);
                    break;
                case ENDER_DRAGON:
                    CorePlugin.economy.depositPlayer(player, 50000);
                    CorePlugin.broadcastPrefixed("&6Gracz " + player.getName() + "pokonal smoka! Gratulacje!");
                    break;
                case WITHER:
                    CorePlugin.economy.depositPlayer(player, 1000);
                    break;
                case ENDERMITE:
                    CorePlugin.economy.depositPlayer(player, 30);
                    break;
                case GUARDIAN:
                    CorePlugin.economy.depositPlayer(player, 5000);
                    CorePlugin.broadcastPrefixed("&6Gracz " + player.getName() + "pokonal straznika! Gratulacje!");
                    break;
                case SNOWMAN:
                    CorePlugin.economy.depositPlayer(player, 30);
                    break;
                case MAGMA_CUBE:
                    CorePlugin.economy.depositPlayer(player, 25);
                    break;
                case WOLF:
                    CorePlugin.economy.depositPlayer(player, 15);
                    break;
                case PIG:
                    CorePlugin.economy.depositPlayer(player, 15);
                    break;
                case COW:
                    CorePlugin.economy.depositPlayer(player, 30);
                    break;
                case MUSHROOM_COW:
                    CorePlugin.economy.depositPlayer(player, 150);
                    break;
                case SHEEP:
                    CorePlugin.economy.depositPlayer(player, 10);
                    break;
            }
        }
    }

}
