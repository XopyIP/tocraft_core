package pl.tocraft.core.listeners;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import pl.tocraft.core.CorePlugin;

import java.util.Collection;

//TODO: skopiowane ze starych toolsow, do poprawki

//Co to kurwa jest ? xD, nie poprawie bo nie ogarne ._.

public class DropListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if (!event.isCancelled()) {
            Player player = event.getPlayer();
            Block block = event.getBlock();
            if (!player.getGameMode().equals(GameMode.CREATIVE)) {
                Collection<ItemStack> playerDrops = block.getDrops(player.getInventory().getItemInHand());
                event.setExpToDrop(0);
                if (block.getType().equals(Material.WHEAT) || block.getType().equals(Material.SEEDS) || block.getType().equals(Material.NETHER_WARTS) || block.getType().equals(Material.MELON_STEM) || block.getType().equals(Material.PUMPKIN_STEM) || block.getType().equals(Material.CARROT) || block.getType().equals(Material.CROPS) || block.getType().equals(Material.POTATO)) {
                    block.breakNaturally();
                    return;
                }
                if (block.getType().equals(Material.LEAVES) || block.getType().equals(Material.LEAVES_2)) {
                    if (event.getPlayer().getItemInHand().getType() == Material.SHEARS) {
                        playerDrops.add(CorePlugin.fixLeavesDrop(new ItemStack(block.getType(), 1, (short) block.getData())));
                    }
                    if (Math.random() * 100.0D < 3) {
                        playerDrops.add(new ItemStack(Material.APPLE, 1));
                        CorePlugin.economy.depositPlayer(player, 3);
                    }
                }
                if (block.getType().equals(Material.SNOW)) {
                    playerDrops.add(new ItemStack(Material.SNOW_BALL, 1));
                }
                if (block.getType().equals(Material.STONE) || block.getType().equals(Material.COBBLESTONE)) {
                    player.giveExp(CorePlugin.randomAmount(1, 3));
                    CorePlugin.economy.depositPlayer(player, 0.05);
                    if ((Math.random() * 100.0D <= CorePlugin.getDropChance("iron"))) {
                        playerDrops.add(new ItemStack(Material.IRON_ORE, 1));
                        player.sendMessage(ChatColor.GRAY + "Znalazles zelazo !");
                        player.giveExp(10);
                        CorePlugin.economy.depositPlayer(player, 5);
                    }

                    if ((Math.random() * 100.0D <= CorePlugin.getDropChance("lapis"))) {
                        int amount = CorePlugin.randomAmount(2, 6);
                        playerDrops.add(new ItemStack(Material.INK_SACK, amount, (short) 4));
                        player.sendMessage(ChatColor.GOLD + "Znalazles " + amount + " sztuki lapis !");
                        player.giveExp(amount * 4);
                        CorePlugin.economy.depositPlayer(player, amount * 2);
                    }

                    if ((Math.random() * 100.0D <= CorePlugin.getDropChance("coal"))) {
                        double fortuneRandom1 = Math.random() * 100.0D;
                        double fortuneRandom2 = Math.random() * 100.0D;
                        double fortuneRandom3 = Math.random() * 100.0D;
                        ItemStack wegiel = new ItemStack(Material.COAL, 1);
                        if ((30.0D <= fortuneRandom1) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 1)) {
                            wegiel.setAmount(2);
                        } else if ((20.0D <= fortuneRandom2) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 2)) {
                            wegiel.setAmount(3);
                        } else if ((10.0D <= fortuneRandom3) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 3)) {
                            wegiel.setAmount(4);
                        }
                        player.sendMessage(ChatColor.GRAY + "Znalazles " + wegiel.getAmount() + " sztuk wegla.");
                        player.giveExp(wegiel.getAmount() * 2);
                        CorePlugin.economy.depositPlayer(player, wegiel.getAmount() * 2.10);
                        playerDrops.add(wegiel);
                    }

                    if (Math.random() * 100.0D <= CorePlugin.getDropChance("gold")) {
                        playerDrops.add(new ItemStack(Material.GOLD_ORE));
                        player.sendMessage(ChatColor.GOLD + "Znalazles zloto !");
                        player.giveExp(12);
                        CorePlugin.economy.depositPlayer(player, 9);
                    }

                    if (Math.random() * 100.0D <= CorePlugin.getDropChance("redstone")) {
                        int amount = CorePlugin.randomAmount(1, 5);
                        playerDrops.add(new ItemStack(Material.REDSTONE, amount));
                        player.sendMessage(ChatColor.RED + "Znalazles " + amount + " redstone !");
                        player.giveExp(amount * 9);
                        CorePlugin.economy.depositPlayer(player, amount * 1.32);
                    }

                    if ((Math.random() * 100.0D <= CorePlugin.getDropChance("emerald"))) {
                        double fortuneRandom1 = Math.random() * 100.0D;
                        double fortuneRandom2 = Math.random() * 100.0D;
                        double fortuneRandom3 = Math.random() * 100.0D;
                        ItemStack emerald = new ItemStack(Material.EMERALD, 1);
                        if ((30.0D <= fortuneRandom1) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 1)) {
                            emerald.setAmount(CorePlugin.randomAmount(2, 3));
                        } else if ((20.0D <= fortuneRandom2) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 2)) {
                            emerald.setAmount(CorePlugin.randomAmount(2, 4));
                        } else if ((10.0D <= fortuneRandom3) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 3)) {
                            emerald.setAmount(CorePlugin.randomAmount(2, 5));
                        }
                        player.sendMessage(ChatColor.GREEN + "Znalazles " + emerald.getAmount() + " szmaragd !");
                        player.giveExp(emerald.getAmount() * 40);
                        CorePlugin.economy.depositPlayer(player, emerald.getAmount() * 6.32);
                        playerDrops.add(emerald);
                    }

                    if (Math.random() * 100.0D <= CorePlugin.getDropChance("diamond")) {
                        double fortuneRandom1 = Math.random() * 100.0D;
                        double fortuneRandom2 = Math.random() * 100.0D;
                        double fortuneRandom3 = Math.random() * 100.0D;
                        ItemStack diamond = new ItemStack(Material.DIAMOND, 1);
                        if ((30.0D <= fortuneRandom1) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 1)) {
                            diamond.setAmount(CorePlugin.randomAmount(2, 3));
                        } else if ((20.0D <= fortuneRandom2) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 2)) {
                            diamond.setAmount(CorePlugin.randomAmount(2, 5));
                        } else if ((10.0D <= fortuneRandom3) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 3)) {
                            diamond.setAmount(CorePlugin.randomAmount(2, 6));
                        }
                        player.sendMessage(ChatColor.AQUA + "Znalazles " + diamond.getAmount() + " diament !");
                        player.giveExp(diamond.getAmount() * 50);
                        CorePlugin.economy.depositPlayer(player, diamond.getAmount() * 6.34);
                        playerDrops.add(diamond);
                    }
                }
                block.setType(Material.AIR);
                CorePlugin.givePlayerDrop(playerDrops, player);
                CorePlugin.recalculateDurability(player);
            }
        }
    }
}
