package pl.tocraft.core.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by artur on 19.08.15.
 */
public class TreasureChestListener implements Listener {

    // YourSenseiCreeper przed chwilą - YourSenseiCreeper: artur daj Random r = new Random() i potem lista.get(r.getInt(lista.size())

    private Random random = new Random();
    private ArrayList<ItemStack> items = new ArrayList<>();

    @EventHandler
    public void onRightClick(PlayerInteractEvent event){
        //todo: zamiast co klik ogarniac to od nowa, zrobic to raz ;)
        //Kod do dópy!
        
        //Przerwie metode gdy akcja nie bedzie ppm na air lub na blok
        if(event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != RIGHT_CLICK_BLOCK)
            return;
        
        Player player = event.getPlayer();
        if(event.getPlayer().getItemInHand().getType() == Material.CHEST){
            //przerwie gdy item nie bedzie mial itemmety
            if(!event.getPlayer().getItemInHand().hasItemMeta())
                return;
            ItemMeta meta = event.getPlayer().getItemInHand().getItemMeta();
            if(meta.getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Skrzynia Skarbow")){
                items.clear();
                items.add(new ItemStack(Material.DIAMOND, 16));
                items.add(new ItemStack(Material.EMERALD, 16));
                //player.getInventory().remove(event.getPlayer().getItemInHand());
                int index = random.nextInt(items.size());
                player.getInventory().removeItem(event.getPlayer().getItemInHand());
                player.getInventory().addItem(items.get(index));
            }
        }
    }
}