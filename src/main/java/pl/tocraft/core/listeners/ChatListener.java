package pl.tocraft.core.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import pl.mrgregorix.jsonchatapi.ChatComponent;
import pl.tocraft.core.CorePlugin;

/**
 * Created by Artur on 2015-04-10.
 */

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        event.setCancelled(true);
        if (CorePlugin.isChatLocked()) {
            if (!event.getPlayer().hasPermission("tocraft.chat.bypass")) {
                event.getPlayer().sendMessage(CorePlugin.getTag() + ChatColor.RED + "Czat jest zablokowany.");
                return;
            }
        }
        if (event.getPlayer().hasPermission("tocraft.chat.colour")) {
            event.setMessage(event.getMessage().replaceAll("&", "" + ChatColor.COLOR_CHAR));
        }

        String format = ChatColor.GRAY + CorePlugin.chat.getPlayerPrefix(event.getPlayer()).replaceAll("&", "" + ChatColor.COLOR_CHAR) + event.getPlayer().getDisplayName() + ChatColor.WHITE + ": " + event.getMessage();

        CorePlugin.playerForEach(p -> ChatComponent.fromText(format).send(p.getPlayer()));
    }
}
