package pl.tocraft.core.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPortalEvent;
import pl.tocraft.core.CorePlugin;

public class PlayerPortalListener implements Listener {
    @EventHandler
    public void onPortal(PlayerPortalEvent event) {
        event.setCancelled(true);
        if (event.getFrom().getWorld().getName().equalsIgnoreCase("world")) {
            CorePlugin.sendPrefixed(event.getPlayer(), "Przeteleportowano do piekla.");
            event.getPlayer().teleport(Bukkit.getWorld("world_nether").getSpawnLocation());
        } else if (event.getFrom().getWorld().getName().equalsIgnoreCase("world_nether")) {
            CorePlugin.sendPrefixed(event.getPlayer(), "Przeteleportowano na spawn.");
            event.getPlayer().teleport(Bukkit.getWorld("world").getSpawnLocation());
        } else {
            CorePlugin.sendPrefixed(event.getPlayer(), "Do piekla mozna wejsc tylko portalem znajdujacym sie na spawnie.");
        }
    }
}