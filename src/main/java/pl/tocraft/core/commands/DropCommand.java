package pl.tocraft.core.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlugin;

public class DropCommand extends Command {

    public DropCommand() {
        super(new String[]{"drop"});
        super.setDescription("Informacje na temat dropu ze stone");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        sender.sendMessage(CorePlugin.getTag() + ChatColor.WHITE + "Drop ze stone oraz cobblestone:");
        sender.sendMessage(ChatColor.AQUA + "Diament: " + ChatColor.RED + CorePlugin.getDropChance("diamond") + "%");
        sender.sendMessage(ChatColor.AQUA + "Szmaragd: " + ChatColor.RED + CorePlugin.getDropChance("emerald") + "%");
        sender.sendMessage(ChatColor.AQUA + "Zloto: " + ChatColor.RED + CorePlugin.getDropChance("gold") + "%");
        sender.sendMessage(ChatColor.AQUA + "Zelazo: " + ChatColor.RED + CorePlugin.getDropChance("iron") + "%");
        sender.sendMessage(ChatColor.AQUA + "Redstone: " + ChatColor.RED + CorePlugin.getDropChance("redstone") + "%");
        sender.sendMessage(ChatColor.AQUA + "Lapis: " + ChatColor.RED + CorePlugin.getDropChance("lapis") + "%");
        sender.sendMessage(ChatColor.AQUA + "Wegiel: " + ChatColor.RED + CorePlugin.getDropChance("coal") + "%");
    }

}
