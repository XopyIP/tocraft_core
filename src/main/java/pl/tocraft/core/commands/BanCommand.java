package pl.tocraft.core.commands;

import org.apache.commons.lang.StringUtils;
import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.themolka.cmds.command.UsageException;
import pl.tocraft.core.CorePlugin;

public class BanCommand extends Command {

    public BanCommand() {
        super(new String[]{"ban"});
        setDescription("Zbanuj gracza");
        setPermission("tocraft.core.ban");
        setUsage("<gracz> [powod]");
    }

    //Skopiowane z bukkita!
    //Todo: poprawic na swoj sposob XD

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        if (args.length == 0) {
            throw new UsageException(null);
        }

        String reason = args.length > 0 ? StringUtils.join(args, ' ', 1, args.length) : null;
        Bukkit.getBanList(BanList.Type.NAME).addBan(args[0], reason, null, sender.getName());

        Player player = Bukkit.getPlayer(args[0]);
        if (player != null) {
            player.kickPlayer("Zostales zbanowany.");
        }
        Bukkit.broadcastMessage(CorePlugin.getTag() + ChatColor.GOLD + "Gracz " + ChatColor.RED + player.getName() + ChatColor.GOLD + " zostal zbanowany.");
    }

}
