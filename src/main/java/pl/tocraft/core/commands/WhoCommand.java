package pl.tocraft.core.commands;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlugin;

import java.util.List;
import java.util.stream.Collectors;

public class WhoCommand extends Command {

    public WhoCommand() {
        super(new String[]{"who", "list", "online", "graczeonline"});
        super.setDescription("Lista graczy online");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        CorePlugin.sendPrefixed(sender, "Na serwerze znajduje sie obecnie " + Bukkit.getOnlinePlayers().size() + " graczy online.");
        List<String> players = Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
        sender.sendMessage(StringUtils.join(players, ", "));
    }

}
