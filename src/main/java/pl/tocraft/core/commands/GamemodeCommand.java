package pl.tocraft.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.themolka.cmds.command.UsageException;
import pl.tocraft.core.CorePlugin;
import pl.tocraft.core.McPlayer;

import java.util.HashMap;

public class GamemodeCommand extends Command {

    private static HashMap<String, GameMode> gamemodes = new HashMap<>();

    static {
        gamemodes.put("0", GameMode.SURVIVAL);
        gamemodes.put("1", GameMode.CREATIVE);
        gamemodes.put("2", GameMode.ADVENTURE);
        gamemodes.put("3", GameMode.SPECTATOR);
        gamemodes.put("s", GameMode.SURVIVAL);
        gamemodes.put("c", GameMode.CREATIVE);
        gamemodes.put("a", GameMode.ADVENTURE);
        gamemodes.put("spec", GameMode.SPECTATOR);
        gamemodes.put("survival", GameMode.SURVIVAL);
        gamemodes.put("creative", GameMode.CREATIVE);
        gamemodes.put("adventure", GameMode.ADVENTURE);
        gamemodes.put("spectator", GameMode.SPECTATOR);
    }

    public GamemodeCommand() {
        super(new String[]{"gamemode", "gm"});
        setDescription("Zmiana trybu gry.");
        setUsage("<gamemode> [player]");
        setPermission("tocraft.core.gamemode");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        if (args.length == 0)
            throw new UsageException(null);
        if (args.length == 2 && !sender.hasPermission("mcwyspa.core.gamemode.others"))
            throw new CommandException(CorePlugin.getTag() + ChatColor.RED + "Nie mozesz zmieniac trybu gry innym gracza!");
        if (args.length < 2 && !(sender instanceof Player))
            throw new UsageException(CorePlugin.getTag() + ChatColor.RED + "Musisz podac nick gracza!");


        GameMode gameMode = gamemodes.get(args[0].toLowerCase());

        if (gameMode == null)
            throw new CommandException(CorePlugin.getTag() + ChatColor.RED + "Nieznany tryb gry!");

        McPlayer target = args.length == 1 ? CorePlugin.getMcPlayer((Player) sender) : CorePlugin.getMcPlayer(Bukkit.getPlayer(args[1]));

        if (target == null)
            throw new CommandException(CorePlugin.getTag() + ChatColor.RED + "Nie znaleziono gracza: " + ChatColor.GREEN + args[1]);

        target.setGameMode(gameMode);

        target.sendPrefixed("&aTwoj tryb gry to teraz: &b" + gameMode.toString().toLowerCase() + "!");

        if (target.getPlayer() != sender)
            CorePlugin.sendPrefixed(sender, "&aTryb gry gracza &b" + target.getName() + "&a to teraz: &b" + gameMode.toString().toLowerCase() + "!");
    }
}
