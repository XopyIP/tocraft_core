package pl.tocraft.core.commands;

import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.themolka.cmds.command.UsageException;
import pl.tocraft.core.CorePlugin;

public class UnbanCommand extends Command {

    public UnbanCommand() {
        super(new String[]{"unban"});
        super.setDescription("Odbanuj gracza");
        super.setPermission("tocraft.core.unban");
        super.setUsage("<gracz>");
    }

    //Skopiowane z bukkita!
    //Todo: poprawic na swoj sposob XD

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        if (args.length == 0) {
            throw new UsageException(null);
        }

        Bukkit.getBanList(BanList.Type.NAME).pardon(args[0]);
        CorePlugin.broadcastPrefixed("&6Gracz &c" + args[0] + " &6zostal odbanowany!");
    }

}
