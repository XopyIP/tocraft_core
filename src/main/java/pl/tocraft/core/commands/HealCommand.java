package pl.tocraft.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.themolka.cmds.command.PermissionException;
import pl.tocraft.core.CorePlugin;

public class HealCommand extends Command {
    public HealCommand() {
        super(new String[]{"heal", "lecz"});
        super.setDescription("Leczenie.");
        setPermission("tocraft.core.heal");
    }

    @Override
    public void handle(Player player, String label, String[] args) throws CommandException {
        if(args.length == 0){
            player.setHealth(20);
            player.setFoodLevel(20);
            CorePlugin.sendPrefixed(player, "Uleczono!");
        }else{
            if(player.hasPermission(getPermission() + ".other")){
                if(Bukkit.getPlayer(args[0]) != null){
                    Bukkit.getPlayer(args[0]).setHealth(20);
                    Bukkit.getPlayer(args[0]).setFoodLevel(20);
                    CorePlugin.sendPrefixed(Bukkit.getPlayer(args[0]), "Zostales uleczony!");
                    CorePlugin.sendPrefixed(player, "Uleczono gracza " + args[0]);
                }
            }else{
                throw new PermissionException();
            }
        }
    }
}
