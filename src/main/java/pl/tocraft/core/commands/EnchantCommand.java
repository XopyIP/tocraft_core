package pl.tocraft.core.commands;

import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;

public class EnchantCommand extends Command {
    public EnchantCommand() {
        super(new String[]{"enchant"});
        super.setDescription("Mobilny enchanting.");
        setPermission("tocraft.core.enchant");
    }

    @Override
    public void handle(Player player, String label, String[] args) throws CommandException {
        player.openEnchanting(player.getLocation(), true);
    }
}
