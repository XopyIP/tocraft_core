package pl.tocraft.core.commands;

import org.bukkit.BanEntry;
import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlugin;

public class BanlistCommand extends Command {

    public BanlistCommand() {
        super(new String[]{"banlist"});
        super.setDescription("Lista banow");
        super.setPermission("tocraft.core.banlist");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {

        StringBuilder message = new StringBuilder();
        BanEntry[] banlist = Bukkit.getBanList(BanList.Type.NAME).getBanEntries().toArray(new BanEntry[0]);

        for (int x = 0; x < banlist.length; x++) {
            if (x != 0) {
                message.append(", ");
            }

            message.append(banlist[x].getTarget());
        }

        sender.sendMessage(CorePlugin.getTag() + ChatColor.GOLD + "Jest " + banlist.length + " banow:");
        sender.sendMessage(message.toString());
    }

}
