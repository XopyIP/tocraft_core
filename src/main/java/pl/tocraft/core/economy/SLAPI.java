package pl.tocraft.core.economy;

import pl.tocraft.core.ConfigManager;

import java.util.UUID;

public class SLAPI {
    public static void saveBalances() {
        for (UUID uuid : EconomyAPI.getBalanceMap().keySet()) {
            ConfigManager.getConfig("balance").set("balance." + uuid, EconomyAPI.getBalanceMap().get(uuid));
        }
        ConfigManager.save("balance");
    }


    public static void loadBalances() {
        if (!ConfigManager.getConfig("balance").contains("balance")) return;
        for (String uuid : ConfigManager.getConfig("balance").getConfigurationSection("balance").getKeys(false)){
            EconomyAPI.setBalance(UUID.fromString(uuid), ConfigManager.getConfig("balance").getDouble("balance." + UUID.fromString(uuid)));
        }
    }
}
