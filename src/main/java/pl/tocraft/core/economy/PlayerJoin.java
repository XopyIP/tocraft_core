package pl.tocraft.core.economy;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        if (EconomyAPI.hasAccount(event.getPlayer().getUniqueId())) return;
        EconomyAPI.setBalance(event.getPlayer().getUniqueId(), 0);
    }
}
