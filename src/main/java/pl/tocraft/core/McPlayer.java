package pl.tocraft.core;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

/**
 * Created by Artur on 2015-04-10.
 * <p>
 * Base representation of player class used by plugin
 */
public final class McPlayer {
    private final Player player;
    private boolean vanished;

    McPlayer(Player player) { //Only Core can create new instance
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public boolean isVanished() {
        return vanished;
    }

    public void setVanished(final boolean vanished) {
        this.vanished = vanished;
    }

    /**
     * Colorize and send message to player
     */
    public void sendColored(final String message) {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    /**
     * Send colored message with prefix
     */
    public void sendPrefixed(final String message) {
        sendColored(CorePlugin.getTag() + message);
    }

    public String getName() {
        return player.getName();
    }

    public void setGameMode(GameMode gameMode) {
        player.setGameMode(gameMode);
    }
}
