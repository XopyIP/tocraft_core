package pl.tocraft.core.recipes;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

public class BowOfTocraft
{
    public BowOfTocraft(){
        ItemStack item = new ItemStack(Material.BOW, 1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "Bow of ToCraft");
        meta.addEnchant(Enchantment.ARROW_DAMAGE, 10, true);
        meta.addEnchant(Enchantment.ARROW_FIRE, 1, true);
        meta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
        meta.addEnchant(Enchantment.ARROW_KNOCKBACK, 3, true);
        meta.addEnchant(Enchantment.LOOT_BONUS_MOBS, 5, true);
        meta.addEnchant(Enchantment.DURABILITY, 10, true);
        item.setItemMeta(meta);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("###", "#$#", "###");
        recipe.setIngredient('#', Material.NETHER_STAR);
        recipe.setIngredient('$', Material.BOW);
        Bukkit.getServer().addRecipe(recipe);
    }
}
