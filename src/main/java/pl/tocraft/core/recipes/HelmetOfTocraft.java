package pl.tocraft.core.recipes;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

public class HelmetOfTocraft
{
    public HelmetOfTocraft(){
        ItemStack item = new ItemStack(Material.DIAMOND_HELMET, 1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "Helmet of ToCraft");
        meta.addEnchant(Enchantment.OXYGEN, 1, true);
        meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 10, true);
        meta.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 10, true);
        meta.addEnchant(Enchantment.PROTECTION_FIRE, 10, true);
        meta.addEnchant(Enchantment.PROTECTION_PROJECTILE, 10, true);
        meta.addEnchant(Enchantment.DURABILITY, 10, true);
        item.setItemMeta(meta);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("###", "#$#", "###");
        recipe.setIngredient('#', Material.NETHER_STAR);
        recipe.setIngredient('$', Material.DIAMOND_HELMET);
        Bukkit.getServer().addRecipe(recipe);
    }
}
