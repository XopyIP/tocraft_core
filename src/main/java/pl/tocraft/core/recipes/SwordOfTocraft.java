package pl.tocraft.core.recipes;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

public class SwordOfTocraft
{
    public SwordOfTocraft(){
        ItemStack item = new ItemStack(Material.DIAMOND_SWORD, 1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "Sword of ToCraft");
        meta.addEnchant(Enchantment.DAMAGE_ALL, 10, true);
        meta.addEnchant(Enchantment.DAMAGE_UNDEAD, 10, true);
        meta.addEnchant(Enchantment.DURABILITY, 10, true);
        item.setItemMeta(meta);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("###", "#$#", "###");
        recipe.setIngredient('#', Material.NETHER_STAR);
        recipe.setIngredient('$', Material.DIAMOND_SWORD);
        Bukkit.getServer().addRecipe(recipe);
    }
}
